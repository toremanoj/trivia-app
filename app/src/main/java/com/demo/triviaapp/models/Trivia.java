package com.demo.triviaapp.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.demo.triviaapp.BR;

import java.util.List;

public class Trivia extends BaseObservable implements Parcelable {

    private int id;
    private String userName;
    private List<TriviaInfo> listTriviaInfo;
    private String dateTime;

    public Trivia(Parcel in) {
        id = in.readInt();
        userName = in.readString();
        dateTime = in.readString();
    }

    public static final Creator<Trivia> CREATOR = new Creator<Trivia>() {
        @Override
        public Trivia createFromParcel(Parcel in) {
            return new Trivia(in);
        }

        @Override
        public Trivia[] newArray(int size) {
            return new Trivia[size];
        }
    };

    public Trivia() {

    }

    @Bindable
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
        notifyPropertyChanged(BR.id);
    }
    @Bindable
    public String getUserName() {
        return userName!=null ? userName : "";
    }

    public void setUserName(String userName) {
        this.userName = userName;
        notifyPropertyChanged(BR.userName);
    }
    @Bindable
    public List<TriviaInfo> getListTriviaInfo() {
        return listTriviaInfo;
    }

    public void setListTriviaInfo(List<TriviaInfo> listTriviaInfo) {
        this.listTriviaInfo = listTriviaInfo;
        notifyPropertyChanged(BR.listTriviaInfo);
    }
    @Bindable
    public String getDateTime() {
        return dateTime!=null ? dateTime : "";
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
        notifyPropertyChanged(BR.dateTime);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(userName);
        dest.writeString(dateTime);
    }
}
