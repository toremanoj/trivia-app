package com.demo.triviaapp.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.demo.triviaapp.BR;

public class TriviaInfo extends BaseObservable implements Parcelable {
    private String question;
    private String answer;

    public TriviaInfo(Parcel in) {
        question = in.readString();
        answer = in.readString();
    }

    public static final Creator<TriviaInfo> CREATOR = new Creator<TriviaInfo>() {
        @Override
        public TriviaInfo createFromParcel(Parcel in) {
            return new TriviaInfo(in);
        }

        @Override
        public TriviaInfo[] newArray(int size) {
            return new TriviaInfo[size];
        }
    };

    public TriviaInfo() {

    }

    @Bindable
    public String getQuestion() {
        return question!=null ? question : "";
    }

    public void setQuestion(String question) {
        this.question = question;
        notifyPropertyChanged(BR.question);
    }

    @Bindable
    public String getAnswer() {
        return answer!=null ? answer : "";
    }

    public void setAnswer(String answer) {
        this.answer = answer;
        notifyPropertyChanged(BR.answer);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(question);
        dest.writeString(answer);
    }
}
