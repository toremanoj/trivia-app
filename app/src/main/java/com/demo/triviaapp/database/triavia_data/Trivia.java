package com.demo.triviaapp.database.triavia_data;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "tbl_trivia")
public class Trivia {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String userName;
    private String triviaInfo;
    private String dateTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTriviaInfo() {
        return triviaInfo;
    }

    public void setTriviaInfo(String triviaInfo) {
        this.triviaInfo = triviaInfo;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }
}
