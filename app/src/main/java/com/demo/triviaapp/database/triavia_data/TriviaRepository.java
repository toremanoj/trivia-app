package com.demo.triviaapp.database.triavia_data;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.room.Room;

import com.demo.triviaapp.database.MyDataBase;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.inject.Inject;

import dagger.hilt.android.qualifiers.ApplicationContext;

public class TriviaRepository {
    private String DB_NAME = "db_trivia";
    private MyDataBase myDataBase;

    @Inject
    public TriviaRepository(@ApplicationContext Context context) {
        myDataBase = Room.databaseBuilder(context, MyDataBase.class, DB_NAME).build();
    }

    public void insertTrivia(final Trivia trivia) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> myDataBase.triviaDao().insert(trivia));
    }

    public LiveData<List<Trivia>> getTriviaHistory(){
        return myDataBase.triviaDao().fetchAll();
    }
}
