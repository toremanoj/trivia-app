package com.demo.triviaapp.database.triavia_data;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.lifecycle.LiveData;
import androidx.room.Query;

import java.util.List;

@Dao
public interface TriviaDao {
    @Insert
    Long insert(Trivia trivia);

    @Query("SELECT * FROM tbl_trivia")
    LiveData<List<Trivia>> fetchAll();
}
