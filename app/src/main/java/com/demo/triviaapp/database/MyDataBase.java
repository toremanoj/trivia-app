package com.demo.triviaapp.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.demo.triviaapp.database.triavia_data.Trivia;
import com.demo.triviaapp.database.triavia_data.TriviaDao;



@Database(entities = {Trivia.class}, version = 1, exportSchema = false)
public abstract class MyDataBase extends RoomDatabase {
    public abstract TriviaDao triviaDao();
}
