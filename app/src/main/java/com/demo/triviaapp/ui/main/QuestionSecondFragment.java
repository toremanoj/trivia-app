package com.demo.triviaapp.ui.main;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.demo.triviaapp.R;
import com.demo.triviaapp.databinding.FragmentQuestionSecondBinding;
import com.demo.triviaapp.models.Trivia;
import com.demo.triviaapp.models.TriviaInfo;
import com.demo.triviaapp.utils.Utils;

public class QuestionSecondFragment extends Fragment {

    private FragmentQuestionSecondBinding binding;
    private TriviaInfo triviaInfo;
    private Trivia trivia;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_question_second, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        trivia = QuestionFirstFragmentArgs.fromBundle(getArguments()).getTrivia();
        triviaInfo = new TriviaInfo();
        triviaInfo.setQuestion(binding.txtquestion.getText().toString());
        binding.setTriviaInfo(triviaInfo);
        binding.setHandler(new ClickHandler());
    }


    public class ClickHandler{
        public void onNextClick(View view){
            String answer = "";
            if(binding.chkWhite.isChecked()){
                answer = String.format("%s %s,",answer.trim(),binding.chkWhite.getText().toString());
            }

            if(binding.chkOrange.isChecked()){
                answer = String.format("%s %s,",answer.trim(),binding.chkOrange.getText().toString());
            }

            if(binding.chkYellow.isChecked()){
                answer = String.format("%s %s,",answer.trim(),binding.chkYellow.getText().toString());
            }

            if(binding.chkGreen.isChecked()){
                answer = String.format("%s %s,",answer.trim(),binding.chkGreen.getText().toString());
            }

            if(answer.equals("")){
                Toast.makeText(getActivity(), getResources().getString(R.string.err_select_answer),Toast.LENGTH_SHORT).show();
                return;
            }

            answer = Utils.removeLastCharacter(answer);
            if(trivia.getListTriviaInfo()!=null){
                triviaInfo.setAnswer(answer);
                trivia.getListTriviaInfo().add(triviaInfo);
            }
            Navigation.findNavController(view)
                    .navigate(QuestionSecondFragmentDirections.actionQuestionSecondFragmentToSummaryFragment(trivia));
        }

    }
}