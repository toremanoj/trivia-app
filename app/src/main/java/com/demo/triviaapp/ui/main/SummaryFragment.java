package com.demo.triviaapp.ui.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.demo.triviaapp.R;
import com.demo.triviaapp.ui.main.view_models.TriviaViewModel;
import com.demo.triviaapp.adapters.TriviaInfoAdapter;
import com.demo.triviaapp.databinding.FragmentSummaryBinding;
import com.demo.triviaapp.models.Trivia;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class SummaryFragment extends Fragment {

    private FragmentSummaryBinding binding;
    private Trivia trivia;
    private RecyclerView recyclerTriviaInfo;
    private TriviaInfoAdapter triviaInfoAdapter;
    TriviaViewModel viewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_summary, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this).get(TriviaViewModel.class);

//        viewModel = new ViewModelProvider(this, ViewModelProvider
//                .AndroidViewModelFactory.getInstance(getActivity().getApplication())).get(TriviaViewModel.class);
        trivia = SummaryFragmentArgs.fromBundle(getArguments()).getTrivia();
        binding.setTrivia(trivia);
        binding.setHandler(new ClickHandler());
        initRecyclerView();
        initAdapter();
    }

    private void initAdapter() {
        triviaInfoAdapter = new TriviaInfoAdapter(getActivity(),trivia.getListTriviaInfo());
        recyclerTriviaInfo.setAdapter(triviaInfoAdapter);
    }

    private void initRecyclerView() {
        recyclerTriviaInfo = binding.recyclerTriviaInfo;
        recyclerTriviaInfo.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
    }

    public class ClickHandler {
        public void onFinishClick(View view) {
            viewModel.insertTrivia(trivia);
            Navigation.findNavController(view).navigate(SummaryFragmentDirections.actionSummaryFragmentToHomeFragment());
        }
    }
}