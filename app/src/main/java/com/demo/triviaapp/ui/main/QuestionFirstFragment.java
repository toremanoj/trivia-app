package com.demo.triviaapp.ui.main;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.demo.triviaapp.R;
import com.demo.triviaapp.databinding.FragmentQuestionFirstBinding;
import com.demo.triviaapp.models.Trivia;
import com.demo.triviaapp.models.TriviaInfo;

import java.util.ArrayList;
import java.util.List;

public class QuestionFirstFragment extends Fragment {

    private FragmentQuestionFirstBinding binding;
    private TriviaInfo triviaInfo;
    private Trivia trivia;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_question_first, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        trivia = QuestionFirstFragmentArgs.fromBundle(getArguments()).getTrivia();
        triviaInfo = new TriviaInfo();
        triviaInfo.setQuestion(binding.txtquestion.getText().toString());
        binding.setTriviaInfo(triviaInfo);
        binding.setHandler(new ClickHandler());

        binding.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.radioSachin:
                        triviaInfo.setAnswer(binding.radioSachin.getText().toString());
                        break;

                    case R.id.radioVirat:
                        triviaInfo.setAnswer(binding.radioVirat.getText().toString());
                        break;

                    case R.id.radioAdam:
                        triviaInfo.setAnswer(binding.radioAdam.getText().toString());
                        break;

                    case R.id.radioJacques:
                        triviaInfo.setAnswer(binding.radioJacques.getText().toString());
                        break;
                }
            }
        });
    }

    public class ClickHandler{
        public void onNextClick(View view){
            if(triviaInfo.getAnswer().equals("")){
                Toast.makeText(getActivity(), getResources().getString(R.string.err_select_answer),Toast.LENGTH_SHORT).show();
                return;
            }
            List<TriviaInfo> triviaInfoList = new ArrayList<>();
            triviaInfoList.add(triviaInfo);
            trivia.setListTriviaInfo(triviaInfoList);
            Navigation.findNavController(view)
                    .navigate(QuestionFirstFragmentDirections.actionQuestionFirstFragmentToQuestionSecondFragment(trivia));
        }
    }
}