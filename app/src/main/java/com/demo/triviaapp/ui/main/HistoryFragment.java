package com.demo.triviaapp.ui.main;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.demo.triviaapp.R;
import com.demo.triviaapp.ui.main.view_models.TriviaViewModel;
import com.demo.triviaapp.adapters.TriviaHistoryAdapter;
import com.demo.triviaapp.databinding.FragmentHistoryBinding;
import com.demo.triviaapp.models.Trivia;
import com.demo.triviaapp.models.TriviaInfo;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class HistoryFragment extends Fragment {
    private FragmentHistoryBinding binding;
    private Trivia trivia;
    private List<Trivia> listTrivia;
    private RecyclerView recyclerHistory;
    private TriviaHistoryAdapter triviaHistoryAdapter;
    TriviaViewModel viewModel;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_history, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.setHandler(new ClickHandler());
        viewModel = new ViewModelProvider(this).get(TriviaViewModel.class);
        getTriviaHistory();
        initRecyclerView();
    }

    private void getTriviaHistory() {
        viewModel.getTrivia().observe(getViewLifecycleOwner(), trivias -> {
            if(trivias!=null && trivias.size()>0){
                listTrivia = new ArrayList<>();
                binding.txtNoRecords.setVisibility(View.GONE);
                binding.recyclerHistory.setVisibility(View.VISIBLE);
                Type type = new TypeToken<ArrayList<TriviaInfo>>(){}.getType();
                for (com.demo.triviaapp.database.triavia_data.Trivia tr :
                        trivias) {
                    trivia = new Trivia();
                    List<TriviaInfo> triviaInfoList = new Gson().fromJson(tr.getTriviaInfo(), type);
                    trivia.setListTriviaInfo(triviaInfoList);
                    trivia.setDateTime(tr.getDateTime());
                    trivia.setUserName(tr.getUserName());
                    trivia.setId(tr.getId());
                    listTrivia.add(trivia);
                }
                initAdapter();
            }else{
                binding.txtNoRecords.setVisibility(View.VISIBLE);
                binding.recyclerHistory.setVisibility(View.GONE);
            }
        });
    }

    private void initAdapter() {
        triviaHistoryAdapter = new TriviaHistoryAdapter(getActivity(),listTrivia);
        recyclerHistory.setAdapter(triviaHistoryAdapter);
    }

    private void initRecyclerView() {
        recyclerHistory = binding.recyclerHistory;
        recyclerHistory.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
    }

    public class ClickHandler{
        public void onBackClick(View view){
            Navigation.findNavController(view).popBackStack();
        }
    }
}