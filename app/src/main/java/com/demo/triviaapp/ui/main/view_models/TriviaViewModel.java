package com.demo.triviaapp.ui.main.view_models;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.demo.triviaapp.database.triavia_data.Trivia;
import com.demo.triviaapp.database.triavia_data.TriviaRepository;
import com.demo.triviaapp.utils.Utils;
import com.google.gson.Gson;

import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;

@HiltViewModel
public class TriviaViewModel extends AndroidViewModel {

    @Inject
    TriviaRepository triviaRepository;

    @Inject
    public TriviaViewModel(@NonNull Application application) {
        super(application);
        triviaRepository = new TriviaRepository(application.getApplicationContext());
    }

    //Insert information in local Database
    public void insertTrivia(com.demo.triviaapp.models.Trivia trivia){
        Trivia entityTrivia = new Trivia();
        entityTrivia.setDateTime(String.format("%s %s",Utils.getCurrentDate("dd MMM"),
                Utils.getCurrentTime("hh:mm a")));
        entityTrivia.setUserName(trivia.getUserName());
        entityTrivia.setTriviaInfo(new Gson().toJson(trivia.getListTriviaInfo()));

        triviaRepository.insertTrivia(entityTrivia);
    }


    //Get list of all the games played
    public LiveData<List<Trivia>> getTrivia(){
        return triviaRepository.getTriviaHistory();
    }
}
