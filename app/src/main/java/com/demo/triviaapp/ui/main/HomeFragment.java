package com.demo.triviaapp.ui.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.demo.triviaapp.R;
import com.demo.triviaapp.databinding.FragmentHomeBinding;
import com.demo.triviaapp.models.Trivia;


public class HomeFragment extends Fragment {

    private FragmentHomeBinding binding;
    private Trivia trivia;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        trivia = new Trivia();
        binding.setTrivia(trivia);
        binding.setHandler(new ClickHandler());
    }

    public class ClickHandler {
        public void onNextClick(View view) {
            if (trivia.getUserName().equals("")) {
                binding.etName.requestFocus();
                binding.etName.setError(getString(R.string.err_enter_name));
                return;
            }

            Navigation.findNavController(view)
                    .navigate(HomeFragmentDirections.actionHomeFragmentToQuestionFirstFragment(trivia));
        }

        public void onHistoryClick(View view){
            Navigation.findNavController(view).navigate(HomeFragmentDirections.actionHomeFragmentToHistoryFragment());
        }
    }
}