package com.demo.triviaapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.demo.triviaapp.R;
import com.demo.triviaapp.databinding.ViewSummaryBinding;
import com.demo.triviaapp.models.TriviaInfo;
import java.util.List;

public class TriviaInfoAdapter extends RecyclerView.Adapter<TriviaInfoAdapter.MyViewHolder> {
    private Context context;
    private List<TriviaInfo> list;

    public TriviaInfoAdapter(Context context, List<TriviaInfo> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewSummaryBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.view_summary,parent,false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        TriviaInfo triviaInfo = list.get(position);
        holder.binding.setTriviaInfo(triviaInfo);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ViewSummaryBinding binding;
        public MyViewHolder(@NonNull ViewSummaryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
