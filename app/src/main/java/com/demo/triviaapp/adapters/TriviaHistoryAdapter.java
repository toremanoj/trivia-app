package com.demo.triviaapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.demo.triviaapp.R;
import com.demo.triviaapp.databinding.ViewHistoryBinding;
import com.demo.triviaapp.databinding.ViewSummaryBinding;
import com.demo.triviaapp.models.Trivia;
import com.demo.triviaapp.models.TriviaInfo;

import java.util.List;

public class TriviaHistoryAdapter extends RecyclerView.Adapter<TriviaHistoryAdapter.MyViewHolder> {
    private Context context;
    private List<Trivia> list;

    public TriviaHistoryAdapter(Context context, List<Trivia> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewHistoryBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.view_history,parent,false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Trivia trivia = list.get(position);
        holder.binding.setTrivia(trivia);
        holder.binding.recyclerTriviaInfo
                .setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
        TriviaInfoAdapter triviaInfoAdapter = new TriviaInfoAdapter(context,trivia.getListTriviaInfo());
        holder.binding.recyclerTriviaInfo.setAdapter(triviaInfoAdapter);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ViewHistoryBinding binding;
        public MyViewHolder(@NonNull ViewHistoryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
